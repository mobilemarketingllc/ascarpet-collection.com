<?php 


include( dirname( __FILE__ ) . '/include/apicaller.php' );
include( dirname( __FILE__ ) . '/include/constant.php' );
include( dirname( __FILE__ ) . '/include/helper.php' );
//include( dirname( __FILE__ ) . '/include/track-leads.php' );    
require_once dirname( __FILE__ ) . '/product-import.php';

/** GTM */
//include( dirname( __FILE__ ) . '/include/gtm-manager.php' ); 
//include( dirname( __FILE__ ) . '/include/track-leads.php' );    
new Main_Processing();
require_once( ABSPATH . "wp-includes/pluggable.php" );



     
global $jal_db_version;
$jal_db_version = '2.8';

function jal_install() {
	global $wpdb;
	global $jal_db_version;

	
	
	$charset_collate = $wpdb->get_charset_collate();

    $dataload = "SET FOREIGN_KEY_CHECKS=0";
    $wpdb->query( $dataload );

	$sql = "DROP TABLE IF EXISTS `wp_product_check`;
            CREATE TABLE `wp_product_check` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `skuid` varchar(100) COLLATE utf8_unicode_ci NOT NULL UNIQUE ,
            `post_id` bigint(20) unsigned NOT NULL UNIQUE,
            PRIMARY KEY (`id`),
            KEY `fk_post_id` (`post_id`),
            CONSTRAINT `fk_post_id` FOREIGN KEY (`post_id`) REFERENCES `wp_posts` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );


    
    $insert = "insert into wp_product_check(skuid,post_id) SELECT meta_value,post_id FROM wp_postmeta WHERE meta_key = 'sku'";
    $wpdb->query( $insert);
    $dataload = "SET FOREIGN_KEY_CHECKS=1;";
    $wpdb->query( $dataload );

    
    if(get_site_option( 'jal_db_version' ) !== false ){
        update_option( 'jal_db_version', $jal_db_version );
    }
    else{
        
        add_option( 'jal_db_version', $jal_db_version );
    }

	
}




function myplugin_update_db_check() {
    
    
    global $jal_db_version;
    if(get_site_option( 'jal_db_version' ) !== false ){
        if ( get_site_option( 'jal_db_version' ) != $jal_db_version ) {
            jal_install();
        }
    }
    else{
        
        jal_install();
    }
    
}

add_action( 'after_setup_theme', 'myplugin_update_db_check' );

if(isset($_GET['draftretailer']) && $_GET['draftretailer']==341 ){

    global $wpdb;
    $location_ids = "00977,44248,47557,44568,23181,40065,37256,24665,42159,47531,43801,48912,01186,35906,39429,00359,49288,31452,37374,00146,35962,36046,07849,37307,22966,49318,00756,46433,41544,41477,19989,01319,49316,49317,49319,00589,04625,01992,01994,02496,02498,02499,19567,22627,22629,22631,22632,22633,22721,22977,24500,24501,24502,49900,36964,31159,36992,00323,09410,05621,41546,42974,27633,39431,00206,00210,40177,48304,02024,41434,34836,43988,30457,01993,40976,42062,36679,39777,40003,41440,43638,13423,13424,35964,38493,48830,48834,44650,07414,19794,23877,28223,28224,28225,36079,42134,22909,00541,18377,01109,49320,00554,19070,01616,25955,27210,02014,24293,19086,43670,39735,27720,39958,01524,48308,02041,41449,42030,26394,07416,36090,16593,01199,45023,19236,41371,01757,02384,48259,09221,36255,06545,00660,36275,48426,00754,06252,43493,47536,47810,30185,02206,37356,42118,15351,18353,38596,49814,20043,39326,41499,00739,36331,00529,22964,23901,43825,23038,02512,36056,01284,00437,01535,37817,31408,42776,44633,44634,00778,00779,00780,00781,22603,22604,24428,36358,39376,27541,40533,39996,48389,11855,19613,30687,43870,36022,15538,29533,37499,29861,48104,35973,49321,35891,39934,41110,41433,36043,41724,02199,02317,31464,49569,11346,15016,02279,41415,38748,38745,38766,39768,00877,36428,36463,38255,48446,45426,45352,48860,36700,24409,00308,07217,39229,01204,00975,00764,35959,39732,13369,37632";
    $query = 'select p.post_title,post_parent,p.ID
    from wp_posts p
    join wp_postmeta pm on pm.post_id = p.id
    where p.post_type = "page" AND
    p.post_title in ('.$location_ids.') AND
    pm.meta_key = "_wp_page_template" AND meta_value="template-location-id.php"';
    $retailer = $wpdb->get_results($query);
    
    echo "<pre>";
    //var_dump($retailer);exit;
    foreach($retailer as $key=>$value){
        $child = 'select p.ID
                    from wp_posts p
                    where p.post_type = "page" AND
                    p.post_parent = '.$retailer[$key]->ID; 
                    $childern = $wpdb->get_results($child);
                    
                    $ids = array();
                    echo $retailer[$key]->ID;
                    echo $retailer[$key]->post_title;
                    
                    foreach($childern as $key1){
                        array_push($ids,$key1->ID);
                    }
                   $string = implode(",",$ids);
                   var_dump($string);
                    $update = 'Update wp_posts set post_status = "draft" where ID in ('.$string.')';
                    var_dump($wpdb->query($update));
                    echo $update;
                
        
    }

}
if(isset($_GET['delretailer']) && $_GET['delretailer']==341 ){

    global $wpdb;
    $location_ids = "13039,39424,01478,15061,01515,09901,24540,37011,43809,38487,39423,00678,31447,01270,43399,18170,07933,02329,00671,26355,18092,47762,01904,37241,36945,02127,09326,14999,46486,44713,47963,17073,15131,15132,04494,01941,39823,23157,42177,00992,17214,36888,23614,49738,14936,25853,00479,25307,41068,27612,36269,01609,17224,14994,29988,36381,01590,36969,24587,21953,08491,27956,35997,09876,39527,36738,14952,29034,28360,02097,37045,24568,39244,36052,30125,13073,24488,39513,24491,15323,37585,48152,19114,31490,39541,15035,23730,31570,02057,27194,40299,06887,36027,36452,31500,31516,04072,23071,31178,26855,48114,26791,16598,47820,36736,43725,07667,14958,39453,47754,01041,00717,35898,18200,01932,18809,41147,26966,36007,20074,43722,24316,08615,35876,47725,39552,15052,44808,15249,19128,15235,42857,48275,27129,24499,27084,43668,31463,48575,13502,18005,00363,02091,01113,36016,24393,14935,36642,24487,18833,28778,39199,41509,40308,38304,38390,39720,08904,37032,04101,42135,25846,01392,37528,18271,41091,19216,41127,00956,44738,15002,36632,15475,31194,47818,24899,39943,24645,24832,43034,01472,41531,39858,30353,35910,24536,22234,38230,24908,00603,48331,17897,06295,19045,30298,24458,15231";
    $query = 'select p.post_title,post_parent,p.ID
    from wp_posts p
    join wp_postmeta pm on pm.post_id = p.id
    where p.post_type = "page" AND
    p.post_title in ('.$location_ids.')AND
    pm.meta_key = "_wp_page_template" AND meta_value="template-location-id.php"';
    $retailer = $wpdb->get_results($query);
    echo "<pre>";
    foreach($retailer as $key=>$value){
        //Delete parent data first:
        $delete = "delete
        p,pm
        from wp_posts p
        join wp_postmeta pm on pm.post_id = p.id
        where p.ID =".$retailer[$key]->post_parent ;
        echo $delete;
        var_dump($wpdb->query($delete));
        //Get the childs
        echo $retailer[$key]->post_title;
        $child = 'select p.ID
                    from wp_posts p
                    join wp_postmeta pm on pm.post_id = p.id
                    where p.post_type = "page" AND
                    p.post_parent = '.$retailer[$key]->ID; 
                    $childern = $wpdb->get_results($child);
                    $ids = array();
                    foreach($childern as $key1){
                        array_push($ids,$key1->ID);
                    }
                   array_push($ids,$retailer[$key]->ID);
                   $string = implode(",",$ids);
                   var_dump($string);
        $delete1 = "delete
            p,pm
            from wp_posts p
            join wp_postmeta pm on pm.post_id = p.id
            where p.ID in (".$string.")" ;
        echo $delete1;
        var_dump($wpdb->query($delete1));
        
    }
    
}
if(isset($_GET['retailerinfo']) && $_GET['retailerinfo']==341 ){
    
    
    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>"test",'client_secret'=>'test');
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
    
    
    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){
        //Retailer Info
        global $wpdb;
        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $retailers = $apiObj->call("https://leadgen.mm-api.agency/fyw/retailers","GET",$inputs,$headers);
        $phoneupdated =$noretainlerfound =$nophonenumbers =0;
        foreach($retailers as $key => $value){   
            
            
            foreach($retailers[$key]['locations'] as $location){
                
                //var_dump("LOc ID ".$location['locationCode']." PHONE ".$location['forwardingPhone']);
               /* if($location['locationCode'] == "39070"){
                $location['forwardingPhone'] ="517-349-4302";
              } */ 
               if($location['forwardingPhone'] !="")
                {
                  
                    $skus = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_id_api' AND meta_value=".$location['locationCode']);
                    //var_dump($skus);
                    if(count($skus) > 0){

                        $post_id = $skus[0]->post_id;

                        $phone_key = $wpdb->get_results(" SELECT post_id FROM wp_postmeta WHERE meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id=".$post_id);
                        if(count($phone_key) > 0){
                            $update = "UPDATE {$wpdb->postmeta} set meta_value='".$location['forwardingPhone']."'   WHERE  meta_key = 'wpsl_retailer_forwardingphnumber' AND post_id = '".$post_id."'";
                            $wpdb->query( $update );
                            $phoneupdated++;
                        }else{

                           $insert =  "insert into {$wpdb->postmeta} (post_id,meta_key,meta_value) values (".$post_id.",'wpsl_retailer_forwardingphnumber','".$location['forwardingPhone']."') ";    
                        //   var_dump( $insert);
                           $wpdb->query( $insert );
                           $phoneupdated++;
                        }

                        

                    }
                    else{
                        var_dump("No recored found ".$location['locationCode']);    
                        $noretainlerfound++;
                        
                    }
                }
                 else{
                   // var_dump("No forwardign number");
                    $nophonenumbers++;
                } 
               
                //$insert = "update table{$wpdb->postmeta} set  into wp_product_check(skuid,post_id) SELECT meta_value,post_id FROM wp_postmeta WHERE meta_key = 'sku'";
                
           }
        }
        
        

    }
    
}
        

if(isset($_POST['layoutopotion'])){
    get_option('layoutopotion')?update_option('layoutopotion',$_POST['layoutopotion']):add_option('layoutopotion',$_POST['layoutopotion']);
}
if(isset($_POST['siteid']) && $_POST['siteid'] !="" && isset($_POST['clientcode']) && $_POST['clientcode'] !=""  && isset($_POST['clientsecret']) && $_POST['clientsecret'] !=""  )
    {
        
            //CALL Authentication API:
            $apiObj = new APICaller;
            $inputs = array('grant_type'=>'client_credentials','client_id'=>$_POST['clientcode'],'client_secret'=>$_POST['clientsecret']);
            $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
            
            
            if(isset($result['error'])){
                $msg =$result['error'];                
                $_SESSION['error'] = $msg;
                $_SESSION["error_desc"] =$result['error_description'];
                
            }
            else if(isset($result['access_token'])){
               

                /* get_option('CLIENT_CODE')!=""?update_option('CLIENT_CODE',$_POST['clientcode']):add_option('CLIENT_CODE',$_POST['clientcode']);
                get_option('SITE_CODE')!=""?update_option('SITE_CODE',$_POST['siteid']):add_option('SITE_CODE',$_POST['siteid']);   */
                get_option('CLIENTSECRET')?update_option('CLIENTSECRET',$_POST['clientsecret']):add_option('CLIENTSECRET',$_POST['clientsecret']);
                get_option('ACCESS_TOKEN')?update_option('ACCESS_TOKEN',$result['access_token']):add_option('ACCESS_TOKEN',$result['access_token']);
                get_option('CDE_ENV')?update_option('CDE_ENV',$_POST['instance-select']):add_option('CDE_ENV',$_POST['instance-select']); 
                get_option('CDE_LAST_SYNC_TIME')?update_option('CDE_LAST_SYNC_TIME',time()):add_option('CDE_LAST_SYNC_TIME',time()); 
                //API Call for Social Icon
                $inputs = array();
               /*  $headers = array('authorization'=>"bearer ".$result['access_token']);
                $sociallinks = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SOCIALURL,"GET",$inputs,$headers);
                
                if(isset($sociallinks['success']) && $sociallinks['success'] == 1 ){
                    $social_json =  json_encode($sociallinks['result']);
                    get_option('social_links') || get_option('social_links')==""?update_option('social_links',$social_json):add_option('social_links',$social_json);   
                }
                else{
                    $msg =$sociallinks['message'];                
                    $_SESSION['error'] = "Error SOCIAL LINKS";
                    $_SESSION["error_desc"] =$msg;
                    //echo $msg;
                    
                } */
                
                //API Call for getting website INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                
                if(isset($website['success']) && $website['success'] ){

                    $website_json = json_encode($website['result']);
                    get_option('website_json')?update_option('website_json',$website_json):add_option('website_json',$website_json);
                    if ( ! function_exists( 'post_exists' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/post.php' );
                    }
                    
                    
                    /* for($i=0;$i<count($website['result']['locations']);$i++){
                        $location_name = isset($website['result']['locations'][$i]['name'])?$website['result']['locations'][$i]['name']:"";

                        if(post_exists($location_name) == 0 && $location_name != "" ){

                            $array = array(
                                'post_title' => $location_name,
                                'post_type' => 'store-locations',
                                'post_content'  => "LOCATIONS",
                                'post_status'   => 'publish',
                                'post_author'   => 0,
                            );
                            $post_id = wp_insert_post( $array );
                            
                            
                        }
                    
                    } */
                  
                    if(isset($website['result']['sites'])){
                        for($k=0;$k<count($website['result']['sites']);$k++){
                            if($website['result']['sites'][$k]['instance'] == ENV){
                                update_option('blogname',$website['result']['sites'][$k]['name']);
                                $gtmid = $website['result']['sites'][$k]['gtmId'];
                                get_option( 'gtm_script_insert')?update_option( 'gtm_script_insert',$gtmid):update_option( 'gtm_script_insert',$gtmid);
                                get_option('CLIENT_CODE')?update_option('CLIENT_CODE',$_POST['clientcode']):add_option('CLIENT_CODE',$_POST['clientcode']);
                                
                                $clientCode = $website['result']['sites'][$k]['clientCode'];
                                $siteCode= $website['result']['sites'][$k]['siteCode'];
                                
                                get_option('SITE_CODE')?update_option('SITE_CODE',$siteCode):add_option('SITE_CODE',$siteCode);
                                get_option('CLIENT_CODE') ==""?update_option('CLIENT_CODE',$clientCode):add_option('CLIENT_CODE',$clientCode);
                                
                            }
                        }
                    }
                    //get_option( 'gtm_script_insert', )
                    
                }
                else{
                    $msg =$website['message'];                
                    //echo $msg;
                } 
               /*  //API Call for getting Contact INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $contacts = $apiObj->call(BASEURL.CONTACTURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                if(isset($contacts['success']) && $contacts['success'] ){

                    $contacts_json = json_encode($contacts['result']);
                    get_option('contacts_json')?update_option('contacts_json',$contacts_json):add_option('contacts_json',$contacts_json);
                    
                    $phone = isset($contacts['result'][0]['phone'])?$contacts['result'][0]['phone']:'';
                    
                    get_option('api_contact_phone')?update_option('api_contact_phone',$phone):update_option('api_contact_phone',$phone);
                
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Error Contact Info";
                    $_SESSION["error_desc"] =$msg;
                } */
                

                //API Call for geting product brand details:
                    
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $products = $apiObj->call(SOURCEURL.get_option('CLIENT_CODE')."/".PRODUCTURL,"GET",$inputs,$headers);
                var_dump($products,SOURCEURL.get_option('CLIENT_CODE')."/".PRODUCTURL);
                
            
                if(isset($products) ){

                    $product_json = json_encode($products);
                    get_option('product_json')?update_option('product_json',$product_json):add_option('product_json',$product_json);
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Product Brand API";
                    $_SESSION["error_desc"] =$msg;
                }
            }

          
            
           
    }
    else{
        $msg = "Please fill all fields";
        //echo $msg;
    }
    
   
   
 /*    
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    //'https://bitbucket.org/user-name/repo-name',
    'https://bitbucket.org/mobilemarketingllc/grandchild-plugin',
	__FILE__,
	'grand-child'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
	'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));
 */


//Optional: Set the branch that contains the stable release.
/* if(ENV == 'dev' || ENV == 'staging')
    $myUpdateChecker->setBranch('dev');
else if(ENV == 'prod')
    $myUpdateChecker->setBranch('master'); */

 function wpc_theme_global() {
    
/*     define( 'postpercol', '3' );
    include( dirname( __FILE__ ) . '/styles.php' );
    // define( 'productdetail_layout', 'box' );    
    define( 'productdetail_layout', 'box' );  
    wp_enqueue_style('lightbox-style', plugins_url('css/lightgallery.min.css', __FILE__));
    wp_enqueue_script('lightbox-js',plugins_url( 'js/lightgallery-all.min.js', __FILE__));
    wp_enqueue_script('script-js',plugins_url( 'js/script.js', __FILE__)); */
    gtm_head_script();
}
add_action('wp_head', 'wpc_theme_global');

function admin_style() {
    wp_enqueue_style('admin-styles', get_template_directory_uri().'-child/product-addon/css/admin.css');
    wp_enqueue_script('script', get_template_directory_uri().'-child/product-addon/js/retailer_v1.js');
}
add_action('admin_enqueue_scripts', 'admin_style');
// This filter replaces a complete file from the parent theme or child theme with your file (in this case the archive page).
// Whenever the archive is requested, it will use YOUR archive.php instead of that of the parent or child theme.
//add_filter ('archive_template', create_function ('', 'return plugin_dir_path(__FILE__)."archive.php";'));

/* function wpc_theme_add_headers () {
    wp_enqueue_style('frontend-styles', plugins_url('css/styles.css', __FILE__));
} */
// These two lines ensure that your CSS is loaded alongside the parent or child theme's CSS
add_action('init', 'wpc_theme_add_headers');
 
// In the rest of your plugin, add your normal actions and filters, just as you would in functions.php in a child theme.

/* function get_custom_post_type_template($single_template) {
    global $post;
      
    if ($post->post_type != 'post') {
         $single_template = dirname( __FILE__ ) . '/product-listing-templates/single-'.$post->post_type.'.php';
    }
    return $single_template;
}
add_filter( 'single_template', 'get_custom_post_type_template' ); */

//Code for adding Menu
//Nikhil Chinchane: 7 Jan 2019
//
function add_my_menu() {
    add_menu_page (
        'Retailer Settings', // page title 
        'Retailer Settings', // menu title
        'manage_options', // capability
        'my-menu-slug',  // menu-slug
        'my_menu_page',   // function that will render its output
        get_template_directory_uri().'-child/product-addon/img/theme-option-menu-icon.png'   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );

    add_submenu_page('my-menu-slug', __('Retailer Product Data'), __('Retailer Product Data'), 'manage_options', 'retailer_product_data', 'retailer_product_data_html');

}
add_action('admin_menu', 'add_my_menu');


//Create Form for client code and site id:
    function Calling_API_form($site,$clientcode) {
        if(isset($_SESSION['error'])){
     ?>
        <div class="notice notice-info error-info is-dismissible woo-info">
            <div class="info-image">
                <p> <img src='<?php echo get_template_directory_uri()."-child/img/error.png";?>' width="48px"/></p>
            </div>
            <div class="info-descriptions">
                <div class="info-descriptions-title">
                        <h3><strong><?php echo ucfirst($_SESSION["error"]);?></strong></h3>
                </div>
                <p><?php echo $_SESSION["error_desc"];?></p>
                <p><b>Please Contact Plugin Development Team for further details</b></p>
            </div>
        </div>
        <?php
        }
        $form = '
        <div id="wpcontent1" class="client_info_wrap">
            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
                <table class="form-table">
                    <tr>
                        <th colspan="2"><h4>Enter Retailer Info</h4></th>
                    </tr>
                    <tr>
                        <td colspan="2" style="
                        text-align: right;
                    "><a href="/wp-admin/admin.php?page=my-menu-slug&retailerinfo=341" class="button button-primary">Sync All Forwarding Numbers</a></td>
                    </tr>
                    
                    '.( isset( $msg) ? '
                    <tr>
                        <td colspan="2">
                        <span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> '.$msg.'</span>
                            '.( !isset( $_POST['siteid'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Site ID cannot be blank</span>' : null ).'
                            '.( !isset( $_POST['clientcode'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Client Code cannot be blank</span>' : null ).'
                        </td>
                    </tr>
                    ' : null ).'
                    <tr>
                        <td width="150px">
                            <label for="siteid">Select environment <strong>*</strong></label>
                        </td>';
                        $cde = (get_option('CDE_ENV')) && get_option('CDE_ENV')!="" ?get_option('CDE_ENV'):""; 
                        $lasttimesync = get_option('CDE_LAST_SYNC_TIME')?date("F j, Y, g:i a", get_option('CDE_LAST_SYNC_TIME')):"Not yet sync";
                        if($cde != ""){
                            $form .='<td class="form-group">
                            <select name="instance-select">
                                <option value="prod" '.(  $cde=="prod" ? 'selected=selected' : null ).'>Production</option>
                                <option value="staging" '.(  $cde=="staging" ? 'selected=selected' : null ).'>Staging</option>
                                <option value="dev"  '.(  $cde=="dev" ? 'selected=selected' : null ).'>Development</option>
                            </select>
                        </td></tr>';
                        }
                        else{
                            $form .= '<td class="form-group">
                            <select name="instance-select" required>
                                <option value="">Choose Environment</option>
                                <option value="prod">Production</option>
                                <option value="staging">Staging</option>
                                <option value="dev">Development</option>
                            </select>
                        </td></tr>';
                        }
                        
                    
         $form .= '<tr>
                        <td width="150px">
                            <label for="siteid">Client Code <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="siteid" value="' . ( (get_option('CLIENT_CODE') ) ? get_option('CLIENT_CODE') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientcode">API authcode <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientcode" value="' . ( get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientsecret">API authcode Secret <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientsecret" value="' . ( get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="form-group">
                            <button id="syncdata" type="submit" class="button button-primary" >Sync</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right">Last sync time : '.$lasttimesync.'</td>
                    </tr>
                    
                </table>    
            </form>
           
        </div>
        ';
        echo $form;
        unset($_SESSION["error"]);
        unset($_SESSION["error_desc"]);
        
        
    }

    


function getRetailerInformation(){
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('retailer_details').'
                    
</div>
	';
}
add_shortcode( 'getRetailerInformation', 'getRetailerInformation' );

function my_menu_page() {
    retailerInformation();
}
function retailerInformation() {
        ?>
        <?php  
        if( isset( $_GET[ 'tab' ] ) ) {  
            $active_tab = $_GET[ 'tab' ];  
        } else {
            $active_tab = 'tab_one';
        }
        ?>  
        <div class="wrap" id="grandchild-backend">
            <h2>Retailer Settings</h2>
            <div class="description"></div>

            <?php Calling_API_form('','');?>
            <?php settings_errors(); ?> 

            <?php
                //$details = json_decode(get_option('retailer_details'));
                $website_json =  json_decode(get_option('website_json'));
               // $details = json_decode(get_option('social_links'));
                if($website_json || $details) { ?>
                <table class="widefat striped" border="1" style="border-collapse:collapse;">
                    <!-- <thead>
                        <tr>
                            <th width="25%"><strong>Name</strong></th>
                            <th width="75%"><strong>Values</strong></th>
                        </tr>
                    </thead>
                    <tbody> -->
                <?php
                   
                    /* foreach($website_json as $key => $value){   
                        if(!is_array($value)){
                            echo "<tr><td>".ucfirst($key)."</td><td>".ucfirst($value)."</td></tr>";
                        }   
                        
                    } */
                   /*  echo "<tr><th colspan='2'><strong>Social Platforms</strong></th></tr>";
                    if(isset($details)){
                        foreach($details as $key => $value){   
                            echo "<tr><td>".ucfirst($value->platform)."</td><td>".$value->url."</td></tr>";
                        }
                    } */
                    
                     echo "<tr><th colspan='2'><strong>Site information (".ENV.")</strong></th></tr>";
                    $website =  $website_json;
                    for($i=0;$i<count($website->sites);$i++){
                        if($website->sites[$i]->instance == ENV){
                            foreach($website->sites[$i] as $key => $value){   
                                if($key)
                                echo "<tr><td>".ucfirst($key)."</td><td>".($value)."</td></tr>";

                                if($key == "siteCode")
                                get_option('CLIENT_SITE_ID')?update_option('CLIENT_SITE_ID',$value):add_option('CLIENT_SITE_ID',$value);
                            }
                        }
                   
                } 
                   /*  echo "<tr><th colspan='2'><strong>Locations</strong></th></tr>";
                    $website =  $website_json;
                    if(isset($website->locations)){
                    for($i=0;$i<count($website->locations);$i++){

                        $location_name = isset($website_json->locations[$i]->name)?$website->locations[$i]->name:"";
                
                        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
                        $location_address  = isset($website->locations[$i]->address)?$website->locations[$i]->address:"";
                        $location_address .= isset($website->locations[$i]->city)?" , ".$website->locations[$i]->city:"";
                        $location_address .= isset($website->locations[$i]->state)?" , ".$website->locations[$i]->state:"";
                        $location_address .= isset($website->locations[$i]->postalCode)?" , ".$website->locations[$i]->postalCode:"";
                        
                        
                
                        $location_phone = isset($website->locations[$i]->phone)?$website->locations[$i]->phone:"";
                        echo "<tr><td>Name</td><td><b>".$location_name."</b></td></tr>";
                        echo "<tr><td>Address</td><td>".$location_address."</td></tr>";
                        echo "<tr><td>Phone</td><td>".$website->locations[$i]->phone."</td></tr>";
                        echo "<tr><td>Forwarding phone</td><td>".$website->locations[$i]->forwardingPhone."</td></tr>";
                        echo "<tr><td>License Number</td><td>".$website->locations[$i]->licenseNumber."</td></tr>";
                        $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
                        $openinghrs = "<ul style='display:block;'>";
                        for ($j = 0; $j < count($weekdays); $j++) {
                            $location .= $website->locations[$i]->monday;
                            if (isset($website->locations[$i]->{$weekdays[$j]})) {
                                $openinghrs .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $openinghrs .= "</ul>";
                        echo "<tr><td>Opening Hrs</td><td>".$openinghrs."</td></tr>";
                    }
                } */
                    /* $contacts = json_decode(get_option('website_json'));
                    echo "<tr><th colspan='2'><strong>Contacts</strong></th></tr>";
                    if(isset($contacts->contacts)){

                   
                    if(is_array($contacts->contacts)){
                        
                        for($j=0;$j<count($contacts->contacts);$j++){
                            foreach($contacts->contacts[$j] as $key => $value){   
                                echo "<tr><td>".ucfirst($key)."</td><td>".$value."</td></tr>";
                            }
                        }
                    }
                } */
                  //  echo do_shortcode("[storelocation_address alldata]"); 
                ?>
                    </tbody>
                </table>
                <?php 
                }
                retailer_product_data_html();        
            ?>

            
        </div>
        
        <?php
        
}





function retailer_product_data_html(){
    
    ?>
    <div id="wpcontent1" class="client_info_wrap">
  
       
            <table class="widefat striped" border="1" style="border-collapse:collapse;">
               <tbody>
                   <tr>
                       <th colspan="4">
                           <h4>Retailer Flooring Data</h4>
                       </th>
                   </tr>
                    <tr>
                        <th class="form-group"><strong>Product Type</strong></th>
                        <th class="form-group"><strong>Brand</strong></th>
                        <th class="form-group"><strong>Deal Brand</strong></th>
                        <th class="form-group"><strong>Action</strong></th>
                    </tr>

                       <?php
                            $product_json =  json_decode(get_option('product_json'));                       
                            $brandmapping = array(
                                "carpet"=>"carpeting",
                                "hardwood"=>"hardwood_catalog",
                                "laminate"=>"laminate_catalog",
                                "lvt"=>"luxury_vinyl_tile",
                                "tile"=>"tile_catalog",
                                "waterproof"=>"solid_wpc_waterproof"
                            );
                            for($i=0;$i < count($product_json);$i++){
                                ?>
                                <tr>
                                    <form name="productfrm" action=<?php echo $_SERVER['REQUEST_URI'];?> method="POST">
                                        <td class="form-group"><?php echo ucfirst($product_json[$i]->productType); ?></td>
                                        <?php
                                        
                                            if( strcmp($product_json[$i]->productType,"Carpet")){
                                                ?>
                                                <input type="hidden" name="api_product_data_category" value="<?php echo $brandmapping[$product_json[$i]->productType]?>" />
                                                <input type="hidden" name="api_product_data_brand" value="<?php echo $product_json[$i]->manufacturer;?>" />
                                                <?php
                                            }
                                        ?>
                                        <td class="form-group"><?php echo $product_json[$i]->manufacturer; ?></td>
                                        <td class="form-group"><?php echo "--"; ?></td>
                                        <td class="form-group"><button id="syncdata-pro" type="submit" class="button button-primary">Sync</button></td>
                                    </form>
                                </tr>


                                <?php
                            }

                        ?>
                       
                  
               </tbody>
           </table>
      
   </div>
       <?php
       if(isset($_POST['api_product_data_category'] ) && isset($_POST['api_product_data_brand'] )){
                ob_start();
                $upload = wp_upload_dir();
                $upload_dir = $upload['basedir'];
                $upload_dir = $upload_dir . '/sfn-data';
                if (! is_dir($upload_dir)) {
                    mkdir( $upload_dir, 0700 );
                } 

                $sfnapi_category_array = array_keys($brandmapping,$_POST['api_product_data_category']);
                $sfnapi_category = $sfnapi_category_array[0];
                
                $permfile = $upload_dir.'/'.$_POST['api_product_data_category'].'_'.$_POST['api_product_data_brand'].'.csv';
               //$res = 'https://sfn.mm-api.agency/jbrcttlt/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?status=active&status=pending&status=dropped&status=gone';
               $res = SOURCEURL.get_option('CLIENT_CODE').'/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?status=active&status=pending&status=dropped&status=gone';
               
               $tmpfile = download_url( $res, $timeout = 300 );
                    if(is_file($tmpfile)){
                        var_dump(copy( $tmpfile, $permfile ));
                        unlink( $tmpfile ); 

                        $data_url =   admin_url( 'admin.php?page=retailer_product_data&process=all&main_category='.$_POST['api_product_data_category'].'&product_brand='.$_POST['api_product_data_brand'].'');
//unset($_SESSION['error']);
                     }
                     else{
                        $_SESSION['error'] = "Couldn't find products for this category";
                        
                    }
               

              
          ?>
          <script>
          window.location.href = "<?php echo $data_url; ?>";
          </script>
          <?php exit();}
}
   
